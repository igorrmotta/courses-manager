import React from 'react';
import './page.css';

class AboutPage extends React.Component {
  render() {
    return (
      <div className="content">
        <h1>About</h1>
        <p>This Application uses React, Redux, React Router and a variety of other helpful libraries</p>
      </div>
    );
  }
}

export default AboutPage;
