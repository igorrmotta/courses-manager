import * as courseActions from '../actions/courseActions';

export default function courseReducer(state = [], action) {
  switch (action.type) {
    case courseActions.LOAD_COURSES_SUCCESS:
      return action.courses;

    case courseActions.CREATE_COURSE_SUCCESS:
      return [
        ...state,
        Object.assign({}, action.course)
      ];

    case courseActions.UPDATE_COURSE_SUCCESS:
      return [
        ...state.filter(course => course.id !== action.course.id),
        Object.assign({}, action.course)
      ];

    default:
      return state;
  }
}
