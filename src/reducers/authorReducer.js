import * as authorActions from '../actions/authorActions';

export default function authorReducer(state = [], action) {
  switch (action.type) {
    case authorActions.LOAD_AUTHORS_SUCCESS:
      return action.authors;

    default:
      return state;
  }
}
