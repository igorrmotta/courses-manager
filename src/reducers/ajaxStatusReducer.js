import * as ajaxStatusActions from '../actions/ajaxStatusActions';

function actionTypeEndsInSuccess(type) {
  return type.substring(type.length - 8) === '_SUCCESS';
}

//counts the amount of ajax calls in progress
export default function ajaxStatusReducer(state = 0, action) {
  if (action.type === ajaxStatusActions.BEGIN_AJAX_CALL) {
    return state + 1;
  } else if (action.type === ajaxStatusActions.AJAX_CALL_ERROR
    || actionTypeEndsInSuccess(action.type)) {
    return state - 1;
  }

  return state;
}
