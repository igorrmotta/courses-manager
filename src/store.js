import {combineReducers, createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import courses from './reducers/courseReducer';
import authors from './reducers/authorReducer';
import ajaxCallsInProgress from './reducers/ajaxStatusReducer';

const rootReducer = combineReducers({
  courses,
  authors,
  ajaxCallsInProgress
});

export {rootReducer};

export default function configureStore(initialState) {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(thunk)
  );
}
